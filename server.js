const express = require('express');
const bodyParser = require('body-parser');
const handlebars = require('express-handlebars');
const expressSession = require('express-session');
const MongoStore = require('connect-mongo')(expressSession);
const cookieParser = require('cookie-parser');
const passport = require('passport');

const dbConnection = require('./db/dbConnection');

const setBoardsList = require('./modules/boardMiddleware').setBoardsList;
const requireAuth = require('./modules/authMiddleware').requireAuth;

const app = express();

// connect to mongo
dbConnection.connect();

// setup authentication
app.use(cookieParser());
app.use(expressSession({
    secret: 'cs620chankey',
    store: new MongoStore({ mongooseConnection: dbConnection.connection })
}));
app.use(passport.initialize());
app.use(passport.session());

// setup handlebars view engine
app.engine('handlebars', handlebars({defaultLayout: 'main_logo'}));
app.set('view engine', 'handlebars');

// setup body parser
app.use(bodyParser.urlencoded({extended: false}));

// static resources
app.use(express.static(__dirname + '/public'));
app.use('/uploads', express.static(__dirname + '/uploads'));

/// Routing ///

// Middleware to add the boards list to all routes. It's a bit of overkill for routes with no UI,
// but I don't want to miss anything
app.use(setBoardsList);

// redirect index to boards route
app.get('/', (req, res) => {
    res.redirect('/boards');
});

// routes
const boardsRoute = require('./routes/boardsRoute');
app.use('/boards', boardsRoute);

const loginRoute = require('./routes/loginRoute');
app.use('/login', loginRoute);

const adminRoute = require('./routes/adminRoute');
app.use('/admin', requireAuth, adminRoute); // all calls on /admin route require authentication

// handle requests to invalid routes
app.use((req, res) => {
    res.status(404);
    res.render('404', {
        boards: req.boards
    });
});

// start app
app.listen(3000, () => {
    console.log('http://localhost:3000');
});
