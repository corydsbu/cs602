# CS602chan
CS602 S17 Final Project by Cory Stone

## Description
This project is an image board application running on Node.js/express.js with a MongoDB backend.

## Features
Users are be able to select from different board categories where they can create new threads with an attached image and some comments (image is required for first post in thread). Users can also reply to threads, optionally with an image of their own.

Threads will be listed from top to bottom, with threads having the most recent activity appearing on top. Replies within a thread will be displayed in order from top bottom.

Posting threads and replies will be done anonymously. Users can optionally include a password with their post, which can be used to later modify or delete the post. Deleting a thread will delete all replies with it.

Some users will be administrators. There will be a login page for administrators where they can log in with a username and password. Administrators will have access to administrative pages that allow them to:

1. Change their username/password
2. Create a new administrator account
3. Create/rename/delete board categories

Deleting a board category will delete all threads and replies within. Additionally, administrators will be able to delete any thread or reply (although they cannot modify the contents).

## Design
The application consists of three routes: `/boards`, `/login`, and `/admin`.

* The /boards route handles all functionality related to the general user-facing part of the application - creating, reading, updating, and deleting thread content.
* The /login route handles login for admins.
* The /admin route handles admin operations - creating boards, other admin accounts, and changing username/password.

The express server delegates this functionality to respective Router classes. The router classes are responsible for using node modules to modify/retrieve data and redering the responses. The node modules are the ones interact with the models for reading and updating database models, as well as performing operations like image file management.

## How to run
1. Make sure MongoDB is running. The app uses `cs602chan` as the database with no authentication.
2. `npm install`
3. Create an initial admin account: `node bootstrapAdmin <username> <password>`
4. `node server`

## How to use
When the app runs for the first time, there will be no boards. To create a board:

* Click on the Admin link on the top-right of the page.
* Enter your username and password.
* From the Manage Boards page, enter a board name and click Submit.
* The board will appear at the top of the page.

The board will be listed below. Click on the Edit button to change the board name or delete it.

While you are in the admin section, you can also go to Manage Accounts to modify your username/password, or create new admin accounts.

Now that you've created a board, you can click the board link at the top of the page to go to the board. The form at the top of the board page allows you to create a new thread. The password field is optional - if you want to edit a thread comment or image, you must provide a password when you create the thread, and provide the same password when editing the thread. One exception for Admins - if you are logged in as an admin, you can delete a thread without providing a password. This is for moderation.

The board view lists threads in order of latest activity, and shows the 3 latest replies on each. Clicking on a thread title or the Reply link at the bottom of a thread on the board view will take you to the thread view, where you can see all replies and add new replies. The same rules about passwords applies to replies. While new threads require an image, replies to threads do not.

Within the thead view, each post has an Edit button. Click the button to make the desired changes, the click Save. Or you can use the Delete option to delete the post. Deleting a thread will delete all replies with it. Likewise, as an admin, deleting a board will delete all threads and replies with it.

## Deployment
A live version of the application is running at [cs602chan.net](http://cs602chan.net)

## TODO
* Change out bcrypt-nodejs library for bcrypt-js and use async functions
* Favicon
* Show the time when threads and replies were edited
* Better date formatting -- javascript for local time?
* Use Handlebars partial views
* Show error page for too large filesize
* Show error page for invalid file type
* Use mimetype checking
* Pagination for threads on board view
* Limit on number of comments per thread
* Thumbnail versions of images
* Deleted message replaced with "removed" msg instead of delete + dec count?
* Remove threads after a certain number is reached
* Allow admin to associate an image with a board, to be shown in page header when viewing that board
* Board order can be changed
* Make session secret key configurable
* Make mongo credentials an option
* Deploy with SSL
* Moderator feature aka mods=gods -- users who can moderate threads/replies, but don't have admin-level access (manage boards, accounts, etc)
* Ability to remove images for moderation purposes -- replace with "image removed" image?
* Improved account management -- list accounts + type (admin, moderator), all editable
