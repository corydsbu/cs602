const mongoose = require('mongoose');
const credentials = require("./credentials");

mongoose.Promise = global.Promise;

const dbUrl = 'mongodb://' + credentials.host + ':' + credentials.port + '/' + credentials.database;

// Initiates the DB connection
exports.connect = () => {
    mongoose.connect(dbUrl);
};

// Gets the DB connection
exports.connection = mongoose.connection;
