const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Reply = new Schema({
    createdAt: Date,
    updatedAt: Date,
    comment: String,
    image: String,
    password: String
});

// A thread has it's own subject/comment/image, then a list of replies
let Thread = new Schema({
    boardId: {type: String, index: true}, // Index on boardId for lookup of threads by board
    createdAt: Date,
    updatedAt: Date,
    latestReply: {type: Date, index: true}, // Index of lastestReply for sorting threads
    subject: String,
    comment: String,
    image: String,
    password: String,
    replies: [Reply], // Replies are stored as an array of subdocuments (embedded documents)
    replyCount: {type: Number, default: 0} // For efficiency, keep track of num replies in db rather than always calculating
});

module.exports = mongoose.model('Thread', Thread);