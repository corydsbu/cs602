const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

let Admin = new Schema({
    username: {type: String, unique: true}, // validate that username is unique
    password: String
});

// The 'unique' option is technically not a validator, but a helper to create a unique index
// This plugin makes the unique constraint behave as a validator
Admin.plugin(uniqueValidator);

module.exports = mongoose.model('Admin', Admin);