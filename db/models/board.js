const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Board = new Schema({
    name: String
});

module.exports = mongoose.model('Board', Board);