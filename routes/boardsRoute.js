const router = require('express').Router();
const _ = require('underscore');
const boardsModule = require('../modules/boardsModule');
const threadsModule = require('../modules/threadsModule');
const repliesModule = require('../modules/repliesModule');
const setBoard = require('../modules/boardMiddleware').setBoard;
const uploadImage = require('../modules/imageMiddleware').uploadImage;

// Show main page
router.get('/', (req, res) => {
    res.render('mainView', {
        boards: req.boards,
        title: 'CS602chan',
    });
});

// Show threads on a board
router.get('/:boardId/threads', setBoard, (req, res) => {
    // For the board view, we will only display the 3 latest replies on each thread
    let maxReplies = 3;
    
    threadsModule.getThreads(req.board._id, maxReplies).then(threads => {
        _.each(threads, thread => {
            // If reply count is higher than limit, add a hiddenReplyCount field to the thread
            // object so we can render it propertly
            if (thread.replyCount > maxReplies) {
                thread.hiddenReplyCount = thread.replyCount - maxReplies;
            }
        });
        
        res.render('boardView', {
            boards: req.boards,
            title: req.board.name,
            boardId: req.board._id,
            threads: threads
        });
    });
});

// Show one thread
router.get('/:boardId/threads/:threadId', setBoard, (req, res, next) => {
    threadsModule.getThread(req.params.threadId).then(thread => {
        // If thread does not exist, delegate to 404 middleware
        if (!thread) {
            return next();
        }
        
        res.render('threadView', {
            boards: req.boards,
            boardId: req.board._id,
            title: req.board.name,
            thread: thread
        });
    }).catch(() => {
        return next();
    });
});

// Show edit thread view
router.get('/:boardId/threads/:threadId/edit', setBoard, (req, res, next) => {
    threadsModule.getThread(req.params.threadId).then(thread => {
        // If thread does not exist, delegate to 404 middleware
        if (!thread) {
            return next();
        }
        
        // Using query parameters to display errors/form data
        res.render('editThreadView', {
            boards: req.boards,
            boardId: req.board._id,
            title: req.board.name,
            threadId: req.params.threadId,
            subject: thread.subject,
            createdAt: thread.createdAt,
            comment: req.query.comment || thread.comment,
            image: thread.image,
            error: req.query.error
        });
    }).catch(() => {
        return next();
    });
});

// Show edit reply view
router.get('/:boardId/threads/:threadId/replies/:replyId/edit', setBoard, (req, res, next) => {
    threadsModule.getThread(req.params.threadId).then(thread => {
        let reply = repliesModule.getReply(thread, req.params.replyId);
        
        // If reply does not exist, delegate to 404 middleware
        if (!reply) {
            return next();
        }
        
        // Using query parameters to display errors/form data
        res.render('editReplyView', {
            boards: req.boards,
            boardId: req.board._id,
            title: req.board.name,
            threadId: req.params.threadId,
            subject: thread.subject,
            op: { // op = original post, when editing a reply show the original thread message as well
                createdAt: thread.createdAt,
                comment: thread.comment,
                image: thread.image
            },
            replyId: req.params.replyId,
            createdAt: reply.createdAt,
            comment: req.query.comment || reply.comment,
            image: reply.image,
            error: req.query.error
        });
    }).catch(() => {
        return next();
    });
});

// Create thread
router.post('/:boardId/threads', uploadImage('image'), (req, res) => {
    let thread = {
        subject: req.body.subject,
        comment: req.body.comment,
    };
    
    // Add password to thread if provided
    if (req.body.password) {
        thread.password = req.body.password;
    }
    
    // Add image file name to thread if provided
    if (req.file) {
        thread.image = req.file.filename;
    }
    
    threadsModule.createThread(req.params.boardId, thread).then(thread => {
        res.redirect('/boards/' + req.params.boardId + '/threads/' + thread._id + '#thread');
    })
});

// Edit thread
router.post('/:boardId/threads/:threadId/edit', uploadImage('image'), (req, res) => {
    let thread = {
        comment: req.body.comment
    };
    
    // Update image file name if provided
    if (req.file) {
        thread.image = req.file.filename;
    }
    
    let location = '/boards/' + req.params.boardId + '/threads/' + req.params.threadId;
    
    // Be sure to validate the password before allowing an edit
    threadsModule.validatePassword(req.params.threadId, req.body.password).then(
        () => { return threadsModule.updateThread(req.params.threadId, thread); },
        () => { throw 'Incorrect password'; }
    ).then(
        () => { res.redirect(location + '#thread'); },
        (err) => {
            // In case of error, add message as query params in url
            let encodedComment = encodeURIComponent(req.body.comment);
            let encodedError = encodeURIComponent(err);
            res.redirect(location + '/edit?error=' + encodedError + '&comment=' + encodedComment + '#thread');
        }
    );
});

// Delete thread
router.post('/:boardId/threads/:threadId/delete', (req, res) => {
    let location = '/boards/' + req.params.boardId + '/threads/';
    
    // Be sure to validate the password before allowing delete
    // Authenticated users (admins) can always delete
    threadsModule.validatePassword(req.params.threadId, req.body.password, req.isAuthenticated()).then(
        () => { return threadsModule.deleteThread(req.params.threadId); },
        () => { throw 'Incorrect password'; }
    )
    .then(
        () => { res.redirect(location); },
        (err) => {
            // In case of error, add message as query params in url
            let encodedError = encodeURIComponent(err);
            res.redirect(location + req.params.threadId + '/edit?error=' + encodedError + '#thread');
        }
    );
});

// Create reply
router.post('/:boardId/threads/:threadId/replies', uploadImage('image'), (req, res) => {
    let reply = {
        comment: req.body.comment
    };
    
    // Add password to reply if provided
    if (req.body.password) {
        reply.password = req.body.password;
    }
    
    // Add image file name to reply if provided
    if (req.file) {
        reply.image = req.file.filename;
    }
    
    repliesModule.createReply(req.params.threadId, reply).then((reply) => {
        res.redirect('/boards/' + req.params.boardId + '/threads/' + req.params.threadId + '#' + reply._id);
    });
});

// Edit reply
router.post('/:boardId/threads/:threadId/replies/:replyId/edit', uploadImage('image'), (req, res) => {
    let reply = {
        comment: req.body.comment
    };
    
    // Update image filename if provided
    if (req.file) {
        reply.image = req.file.filename;
    }
    
    // Users are allowed to remove reply images
    if (req.body.removeImage) {
        reply.removeImage = true;
    }
    
    let location = '/boards/' + req.params.boardId + '/threads/' + req.params.threadId;
    
    // Before to validate the password before allowing an edit
    repliesModule.validatePassword(req.params.threadId, req.params.replyId, req.body.password).then(
        () => { return repliesModule.updateReply(req.params.threadId, req.params.replyId, reply); },
        () => { throw 'Incorrect password'; }
    ).then(
        () => { res.redirect(location + '#' + req.params.replyId); },
        (err) => {
            // In case of error, add message as query params in url
            let encodedComment = encodeURIComponent(req.body.comment);
            let encodedError = encodeURIComponent(err);
            res.redirect(location + '/replies/' + req.params.replyId + '/edit?error=' + encodedError + '&comment=' + encodedComment + '#reply');
        }
    );
});

// Delete reply
router.post('/:boardId/threads/:threadId/replies/:replyId/delete', (req, res) => {
    let location = '/boards/' + req.params.boardId + '/threads/' + req.params.threadId;
    
    // Be sure to validate the password before allowing a delete
    // Authenticated users (admins) can always delete
    repliesModule.validatePassword(req.params.threadId, req.params.replyId, req.body.password, req.isAuthenticated()).then(
        () => { return repliesModule.deleteReply(req.params.threadId, req.params.replyId); },
        () => { throw 'Incorrect password'; }
    ).then(
        () => { res.redirect(location + '#thread'); },
        (err) => {
            // In case of error, add message as query params in url
            let encodedError = encodeURIComponent(err);
            res.redirect(location + '/replies/' + req.params.replyId + '/edit?error=' + encodedError + '#reply');
        }
    );
});

module.exports = router;
