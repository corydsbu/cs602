const router = require('express').Router();
const passport = require('passport');

// Show login page or redirect to board management depending on authentication status
router.get('/', (req, res) => {
    res.render('adminLoginView', {
        boards: req.boards,
        title: 'Admin Login',
        error: req.query.error
    });
});

// Perform login
router.post('/', passport.authenticate('local', {
    successRedirect: '/admin/boards/', // Go to board management on successful login
    failureRedirect: '/login?error=' + encodeURIComponent('Invalid username and password') // Show login page with error on failure
}));

module.exports = router;
