const router = require('express').Router();
const passport = require('passport');
const boardsModule = require('../modules/boardsModule');
const adminsModule = require('../modules/adminsModule');

// Show board management
router.get('/boards', (req, res) => {
    res.render('adminManageBoardsView', {
        adminView: true,
        boards: req.boards,
        title: 'Manage boards'
    });
});

// Show edit board view
router.get('/boards/:boardId/edit', (req, res) => {
    boardsModule.getBoard(req.params.boardId).then(board => {
        res.render('adminEditBoardView', {
            adminView: true,
            boards: req.boards,
            title: 'Edit board',
            boardName: board.name
        });
    });
});

// Show account management
router.get('/accounts', (req, res) => {
    res.render('adminManageAccountView', {
        adminView: true,
        boards: req.boards,
        title: 'Manage accounts',
        username: req.user.username,
        editSuccess: req.query.editSuccess,
        editUsername: req.query.editUsername,
        editError: req.query.editError,
        createSuccess: req.query.createSuccess,
        createUsername: req.query.createUsername,
        createError: req.query.createError
    });
});

// Create board
router.post('/boards', (req, res) => {
    let board = {
        name: req.body.name
    };
    
    boardsModule.createBoard(board).then(() => {
        res.redirect('/admin/boards/');
    });
});

// Edit board
router.post('/boards/:boardId/edit', (req, res) => {
    let board = {
        name: req.body.name
    };
    
    boardsModule.updateBoard(req.params.boardId, board).then(() => {
        res.redirect('/admin/boards/');
    });
});

// Delete board
router.post('/boards/:boardId/delete', (req, res) => {
    boardsModule.deleteBoard(req.params.boardId).then(() => {
        res.redirect('/admin/boards/');
    });
});

// Create admin
router.post('/accounts', (req, res) => {
    let admin = {
        username: req.body.username,
        password: req.body.password
    };

    adminsModule.createAdmin(admin).then(() => {
        let encodedMessage = encodeURIComponent('Account created successfully');
        res.redirect('/admin/accounts/?createSuccess=' + encodedMessage + '#create');
    }).catch(() => {
        // In case or error, add message as query params to url
        let encodedUsername = encodeURIComponent(req.body.username);
        let encodedError = encodeURIComponent('Account creation failed, username already in use');
        res.redirect('/admin/accounts/?createUsername=' + encodedUsername + '&createError=' + encodedError + '#create');
    });
});

// Edit admin
router.post('/accounts/edit', (req, res) => {
    let admin = {
        username: req.body.username,
        password: req.body.password
    };

    adminsModule.updateAdmin(req.user._id, admin).then(() => {
        let encodedMessage = encodeURIComponent('Account updated successfully');
        res.redirect('/admin/accounts/?editSuccess=' + encodedMessage + '#edit');
    }).catch(() => {
        // In case of error, add message as query params to url
        let encodedUsername = encodeURIComponent(req.body.username);
        let encodedError = encodeURIComponent('Account edit failed, username already in use');
        res.redirect('/admin/accounts/?editUsername=' + encodedUsername + '&editError=' + encodedError + '#edit');
    });
});



// Perform logout
router.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/');
});

module.exports = router;
