const adminsModule = require('./modules/adminsModule');
const dbConnection = require('./db/dbConnection');

if (process.argv.length < 4) {
    console.log('Error: Please provide username and password');
    console.log('Usage: node bootstrapAdmin <username> <password>');
    process.exit();
}

dbConnection.connect();

let admin = {
    username: process.argv[2],
    password: process.argv[3]
}

adminsModule.createAdmin(admin).then(() => {
    console.log('Admin user created successfully!');
    dbConnection.connection.close();
}).catch((err) => {
    console.log(err);
    dbConnection.connection.close();
});
