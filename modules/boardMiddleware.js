// Use this middleware for setting the boards list and current board

const boardsModule = require('./boardsModule');

// Sets the list of boards on the request
exports.setBoardsList = (req, res, next) => {
    boardsModule.getBoards().then(boards => {
        req.boards = boards;
        next();
    });
};

// Sets the current board on the request
exports.setBoard = (req, res, next) => {
    boardsModule.getBoard(req.params.boardId).then(board => {
        if (board) {
            req.board = board;
            next();
        } else {
            throw false;
        }
    }).catch(() => {
        // If there's any problem finding the board, render 404 page
        res.status(404);
        res.render('404', {
            boards: req.boards
        });
    });
};