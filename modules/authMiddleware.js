// This module is for handling authentication via passport

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const adminsModule = require('./adminsModule');

// Serialize user (sends user ID)
passport.serializeUser((user, done) => {
    done(null, user._id);
});

// Deserialize user (gets user by ID)
passport.deserializeUser((id, done) => {
    adminsModule.getAdmin(id).then(admin => {
        done(null, admin);
    }).catch(err => {
        done(err);
    });
});

// 'local' login strategy
passport.use(new LocalStrategy(
    (username, password, done) => {
        // Lookup admin by username
        adminsModule.getAdminByUsername(username).then(admin => {
            // Check if password is valid
            if (adminsModule.isValidPassword(admin, password)) {
                return done(null, admin);
            }
            
            return done(null, false);
        }).catch(err => {
            return done(err);
        });
    }
));

// Use this middleware function on routes that require authentication (e.g. /admin)
exports.requireAuth = (req, res, next) => {
    if (req.isAuthenticated()) {
        return next();
    }
    
    res.redirect('/login');
};
