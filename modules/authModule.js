const bcrypt = require('bcrypt-nodejs');

// Generates a salted hash for the given password
exports.generateHashedPassword = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync());
};

// Verifies that the given password matches the given hashed password
exports.isValidPassword = (password, hashedPassword) => {
    if (!password || !hashedPassword) {
        return false;
    }
    
    return bcrypt.compareSync(password, hashedPassword);
};
