// This module is for handling file uploads via multer

const multer  = require('multer');
const path = require('path');
const crypto = require('crypto');
const _ = require('underscore');

// Files will be stored in the 'uploads' directory with a random name
let storage = multer.diskStorage({
    destination: 'uploads',
    filename: (req, file, cb) => {
        crypto.pseudoRandomBytes(16, (err, raw) => {
            if (err) {
                return cb(err)
            }
            
            cb(null, raw.toString('hex') + path.extname(file.originalname).toLowerCase());
        });
    }
});

// Only image files should be allowed
let fileFilter = (req, file, callback) => {
    let allowedExts = ['.png', '.jpg', '.jpeg', '.gif'];
    
    var ext = path.extname(file.originalname).toLowerCase();
    
    if (!_.contains(allowedExts, ext)) {
        var err = new Error('File type must be one of ' + allowedExts.join(', '));
        return callback(err);
    }
    
    callback(null, true)
}

// Image size limit is set to 1MB
let limits = {
    fileSize: 1048576 //1MB
}

let upload = multer({
    storage: storage,
    fileFilter: fileFilter,
    limits: limits
});

// Call this method with form input name as middleware from routes where request body contains image
exports.uploadImage = imageParam => upload.single(imageParam);
