const fs = require('fs');
const _ = require('underscore');
const Thread = require('../db/models/thread');
const authModule = require('./authModule');

// Gets the reply our of the provided thread object
exports.getReply = (thread, replyId) => {
    if (!thread) {
        return null;
    }
    
    return _.find(thread.replies, (reply) => {
        return reply._id.toHexString() == replyId;
    });
};

// Creates a new reply
exports.createReply = (threadId, replyData) => {
    let date = new Date();
    
    let reply = {
        createdAt: date,
        comment: replyData.comment,
        image: replyData.image
    };
    
    // Add hashed password if provided
    if (replyData.password) {
        reply.password = authModule.generateHashedPassword(replyData.password);
    }
    
    // Reply needs to be pushed into replies array
    // Increment reply count
    // Set latestReply date on the thread for bumping to top of board
    // Specify operation should return updated thread so we can return new reply
    return Thread.findByIdAndUpdate(
        threadId,
        {$push: {replies: reply}, $inc: {replyCount: 1}, $set: {latestReply: date}},
        {new: true}
    ).exec().then(thread => {
        if (!thread) {
            return null;
        }
        
        return thread.replies[thread.replyCount - 1];
    });
}

// Updates a reply
exports.updateReply = (threadId, replyId, replyData) => {
    let replyUpdate = {
        $set: {
            'replies.$.updatedAt': new Date(),
            'replies.$.comment': replyData.comment
        }
    };
    
    // Update reply image if specified
    // If no image specified, check if image should be removed
    if (replyData.image) {
        replyUpdate.$set['replies.$.image'] = replyData.image;
    } else if (replyData.removeImage) {
        replyUpdate.$unset = {'replies.$.image': ''};
    }
    
    // To save a bit ot time, we only need to project the reply we updated in the response
    return Thread.findOneAndUpdate(
        {_id: threadId, 'replies._id' : replyId},
        replyUpdate,
        {select: {replies: {$elemMatch: {_id: replyId}}}}
    ).exec().then(thread => {
        if (thread) {
            // If a new image is provided, delete any existing image
            let oldImage = thread.replies[0].image;

            if (oldImage && (replyData.image || replyData.removeImage)) {
                fs.unlink('uploads/' + oldImage, (err) => {
                    if (err) console.log(err);
                });
            }
        } 
        
        return Promise.resolve();
    });
}

// Deletes a reply
exports.deleteReply = (threadId, replyId) => {
    // Reply needs to be pulled from replies array
    // Decrement reply count
    // To save a bit of time, we only need to project the reply we deleted in the response
    return Thread.findOneAndUpdate(
        {_id: threadId},
        {$pull: {replies: {_id: replyId}}, $inc: {replyCount: -1}},
        {select: {replies: {$elemMatch: {_id: replyId}}}}
    ).exec().then(thread => {
        if (thread) {
            // Delete any existing image
            let image = thread.replies[0].image;

            if (image) {
                fs.unlink('uploads/' + image, (err) => {
                    if (err) console.log(err);
                });
            }
        }
            
        
        return Promise.resolve();
    });
}

// Validates the given password for a reply. Returns a promise that will be resolved on successful
// password validation, or rejected on failed validation. Specify override=true to resolve
// regardless of password.
exports.validatePassword = (threadId, replyId, password, override) => {
    return new Promise((resolve, reject) => {
        // If override, immediately resolve
        if (override) {
            return resolve();
        }
        
        // If no password, immediately reject
        if (!password) {
            return reject();
        }
        
        // Lookup the thread and project only the reply we are interested in
        Thread.findOne({_id: threadId, 'replies._id': replyId}, 'replies.$').exec().then(thread => {
            // If the thread and reply exist, and the passwords match, resolve
            if (thread) {
                let reply = thread.replies[0];
                
                if (reply) {
                    if (authModule.isValidPassword(password, reply.password)) {
                        return resolve();
                    }
                }
            }
            
            // Unsuccessful, reject
            reject();
        });
    });
}

