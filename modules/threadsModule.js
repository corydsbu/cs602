const fs = require('fs');
const _ = require('underscore');
const Thread = require('../db/models/thread');
const authModule = require('./authModule');

// Use this projection to make sure passwords (even hashed) are not inadvertently exposed
const hidePasswords = {password: 0, 'replies.password': 0};

// Gets all threads on a board, showing at most the 'maxReplies' latest replies
exports.getThreads = (boardId, maxReplies) => {
    let projection = JSON.parse(JSON.stringify(hidePasswords));
    projection.replies = {$slice: -maxReplies};
    
    return Thread.find({boardId: boardId}, projection)
        .sort({latestReply: -1})
        .exec();
};

// Gets an entire thread
exports.getThread = threadId => {
    return Thread.findById(threadId, hidePasswords).exec();
};

// Creates a new thread on the given board
exports.createThread = (boardId, threadData) => {
    let date = new Date();
    let thread = new Thread({
        boardId: boardId,
        createdAt: date,
        latestReply: date,
        subject: threadData.subject,
        comment: threadData.comment,
        image: threadData.image
    });
    
    // Add hashed password if provided
    if (threadData.password) {
        thread.password = authModule.generateHashedPassword(threadData.password);
    }
    
    return thread.save();
};

// Updates a thread
exports.updateThread = (threadId, threadData) => {
    // Indicate the thread update time
    threadData.updatedAt = new Date();
    
    return Thread.findByIdAndUpdate(
        threadId,
        threadData
    ).exec().then(thread => {
        if (thread) {
            // If a new image is provided, delete any existing image
            let oldImage = thread.image;

            if (oldImage && threadData.image) {
                fs.unlink('uploads/' + oldImage, (err) => {
                    if (err) console.log(err);
                });
            }
        }  
        
        return Promise.resolve();
    });
};

// Deletes a thread (and all replies)
exports.deleteThread = threadId => {
    return Thread.findByIdAndRemove(threadId).exec().then(thread => {
        if (thread) {
            // Delete any existing image
            if (thread.image) {
                fs.unlink('uploads/' + thread.image, (err) => {
                    if (err) console.log(err);
                });
            }

            // Delete all reply images. fs.unlink is async, but user doesn't need to wait for this
            // loop, so we'll defer
            _.defer(thread => {
                _.each(thread.replies, reply => {
                    if (reply.image) {
                        fs.unlink('uploads/' + reply.image, (err) => {
                            if (err) console.log(err);
                        });
                    }
                });
            }, thread);
        }
        
        return Promise.resolve();
    });
};

// Validates the given password for a thread. Returns a promise that will be resolved on successful
// password validation, or rejected on failed validation. Specify override=true to resolve
// regardless of password.
exports.validatePassword = (threadId, password, override) => {
    return new Promise((resolve, reject) => {
        // If override, immediately resolve
        if (override) {
            return resolve();
        }
        
        // If no password, immediately reject
        if (!password) {
            return reject();
        }
        
        Thread.findById(threadId).exec().then(thread => {
            // If thread exists and passwords match, resolve
            if (thread) {
                if (authModule.isValidPassword(password, thread.password)) {
                    return resolve();
                }
            }
            
            // Unsuccessful, reject
            reject();
        });
    });
};
