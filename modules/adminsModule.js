const Admin = require('../db/models/admin');
const authModule = require('./authModule');

// Gets an admin
exports.getAdmin = adminId => {
    return Admin.findById(adminId).exec();
};

// Gets an admin by username
exports.getAdminByUsername = username => {
    return Admin.findOne({username: username}).exec();
}

// Creates a new admin
exports.createAdmin = adminData => {
    let admin = new Admin({
        username: adminData.username,
        password: authModule.generateHashedPassword(adminData.password) // Store hashed password
    });
    
    return admin.save();
};

// Updates an admin
exports.updateAdmin = (adminId, adminData) => {
    return Admin.findByIdAndUpdate(
        adminId,
        {
            username: adminData.username,
            password: authModule.generateHashedPassword(adminData.password) // Store hashed password
        },
        { runValidators: true, context: 'query' } // Per mongoose docs, required to run validators with this operation
    ).exec();
};

// Checks if a password is valid for the given admin
exports.isValidPassword = (admin, password) => {
    // If admin or password not provided, fail immediately
    if (!admin || !password) {
        return false;
    }
    
    return authModule.isValidPassword(password, admin.password);
}