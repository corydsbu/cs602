const fs = require('fs');
const _ = require('underscore');
const Board = require('../db/models/board');
const Thread = require('../db/models/thread');

// Gets all boards
exports.getBoards = () => {
    return Board.find({}).exec();
};

// Gets a specific board
exports.getBoard = boardId => {
    return Board.findById(boardId).exec();
};

// Creates a new board
exports.createBoard = boardData => {
    let board = new Board({
        name: boardData.name
    });
    
    return board.save();
};

// Updates a board
exports.updateBoard = (boardId, boardData) => {
    return Board.findByIdAndUpdate(
        boardId,
        {
            name: boardData.name
        }
    ).exec();
};

// Deletes a board. This will also delete all threads, replies, and images on that board.
exports.deleteBoard = boardId => {
    return Board.findByIdAndRemove(boardId).exec().then(() => {
        // Model.remove doesn't call back with removed objects, bug?
        // Will have to do lookup and remove separately..
        // Really we are only interested in seeing the images that need to be deleted
        return Thread.find({boardId: boardId}, 'image replies.image').exec();
    }).then(threads => {
        // Remove all threads from board
        Thread.remove({boardId: boardId}).exec().then(() => {
            // Remove images from each thread
            _.each(threads, thread => {
                if (thread.image) {
                    fs.unlink('uploads/' + thread.image, (err) => {
                        if (err) console.log(err);
                    });
                }

                // Remove images from each reply
                _.each(thread.replies, reply => {
                    if (reply.image) {
                        fs.unlink('uploads/' + reply.image, (err) => {
                            if (err) console.log(err);
                        });
                    }
                });
            });
        });
        
        return Promise.resolve();
    });
};
